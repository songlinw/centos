#Dockerfile
FROM centos67-base

MAINTAINER Zaric <zhangzuoqiang@aliyun.com>

# JAVA Settings
ENV JAVA_HOME /usr/java/jdk1.8.0_60
ENV CLASSPATH .:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV PATH $JAVA_HOME/bin:$PATH
# MySQL Settings
ENV MYSQL_ROOT_PASSWORD 123456
# Charset Settings
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN cd ~ ;\
echo "----------------------------- Environment preparation --------------------------" ;\
  yum -y install tar vim wget ;\
  mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup ;\
  wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo ;\
  wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-6.repo ;\
  yum makecache ;\
  yum -y update ;\
  yum clean all ;\

echo "----------------------------- Install JDK --------------------------" ;\
  cd /tmp ;\
  wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jdk-8u60-linux-x64.rpm ;\
  rpm -Uh /tmp/jdk-8u60-linux-x64.rpm ;\
  rm -rf /tmp/jdk-8u60-linux-x64.rpm ;\

echo "----------------------------- Config JDK --------------------------" ;\
  alternatives --install /usr/bin/java java "${JAVA_HOME}/bin/java" 1 ;\
  alternatives --install /usr/bin/javaws javaws "${JAVA_HOME}/bin/javaws" 1 ;\
  alternatives --install /usr/bin/javac javac "${JAVA_HOME}/bin/javac" 1 ;\

echo "----------------------------- Install MySQL --------------------------" ;\
  yum -y install mysql mysql-server ;\
  yum -y clean all ;\

echo "----------------------------- Start MySQL --------------------------" ;\
  service mysqld start ;\
  sleep 5 ;\
  /usr/bin/mysqladmin -u root password ${MYSQL_ROOT_PASSWORD} ;\
  mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e && \
	  "CREATE DATABASE IF NOT EXISTS ubicboot DEFAULT CHARSET utf8 COLLATE utf8_general_ci;" ;\

echo "----------------------------- Config SSH --------------------------" ;\
  ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key ;\
  ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key ;\
  sed -ri 's/session  required  pam_loginuid.so/#session  required  pam_loginuid.so/g' /etc/pam.d/sshd ;\
  mkdir -p /root/.ssh && chown root.root /root && chmod 700 /root/.ssh ;\
  echo 'root:123456' | chpasswd ;\

echo "----------------------------- Define working directory --------------------------"

CMD ["bash"]

EXPOSE 80
EXPOSE 22
EXPOSE 3306

#End
