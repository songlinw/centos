#!/bin/sh
echo "::> Create centos6.7 based mirror with bash/wget/yum/iputils/iprpute/man/vim tools ::"
febootstrap -i bash -i wget -i yum -i iputils -i iproute -i man -i vim-minimal -i openssh-server -i openssh-clients centos67 centos67-image http://mirrors.aliyun.com/centos/6.7/os/x86_64/
